/*
 * grammar.h
 *
 *  Created on: 20.01.2014
 *      Author: david
 */

#ifndef __MYPARSERLIB_GRAMMAR_H_
#define __MYPARSERLIB_GRAMMAR_H_

#include "core.h"

#include <map>
#include <memory>
#include <string>

namespace MyParserLib {

template<typename Iterator, typename Result>
class Grammar: public Parser<Iterator, Result> {
public:
	virtual ~Grammar() {
	}

	virtual void parse(ParserState<Iterator>& state, Result& res) {
		getRule(startRule)->parse(state, res);
	}

	std::shared_ptr<Parser<Iterator, Result> > getRule(std::string name) {
		typename std::map<std::string, std::shared_ptr<Rule> >::iterator it = rules.find(name);
		if (it != rules.end()) {
			return it->second;
		}

		rules[name] = std::make_shared < Rule > (name, parsers);
		return rules[name];
	}

protected:
	void init(std::string _startRuleName) {
		startRule = _startRuleName;
	}

	void init(const char *_startRuleName) {
		startRule.assign(_startRuleName);
	}

	void insert(std::string name, std::shared_ptr<Parser<Iterator, Result> > rule) {
		typename std::map<std::string, std::shared_ptr<Rule> >::iterator it = rules.find(name);
		if (it == rules.end()) {
			rules[name] = std::make_shared < Rule > (name, parsers);
		}

		parsers[name] = rule;
	}

private:
	class Rule: public Parser<Iterator, Result> {
	public:
		Rule(std::string _name, std::map<std::string, std::shared_ptr<Parser<Iterator, Result> > >& _parsers) :
				name(_name), parsers(_parsers) {
		}

		virtual ~Rule() {
		}

		virtual void parse(ParserState<Iterator>& state, Result& res) {
			parsers[name]->parse(state, res);
		}

	private:
		std::string name;
		std::map<std::string, std::shared_ptr<Parser<Iterator, Result> > >& parsers;
	};

	std::map<std::string, std::shared_ptr<Rule> > rules;
	std::map<std::string, std::shared_ptr<Parser<Iterator, Result> > > parsers;
	std::string startRule;
};

}

#endif /* __MYPARSERLIB_GRAMMAR_H_ */
