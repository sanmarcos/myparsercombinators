/*
 * test_charparser.cpp
 *
 *  Created on: 27.01.2014
 *      Author: David Kofler
 */

#include "charparsers.h"

//#include "lists.h"

using namespace MyParserLib;

#define BOOST_TEST_MODULE MyParserLib_Charparser test
#include <boost/test/included/unit_test.hpp>

#include <string>
using namespace std;

BOOST_AUTO_TEST_CASE(int_parser_test) {
	string s1(" 120");
	int i;
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), Char::int_<string>(), i));
	BOOST_CHECK_EQUAL(120, i);

	string s2("dafuq");
	BOOST_REQUIRE(!parse(s2.begin(), s2.end(), Char::int_<string>()));
}

BOOST_AUTO_TEST_CASE(alpha_parser_test) {
	string s1("a");

	char c;
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), Char::alpha<string>(), c));
	BOOST_CHECK_EQUAL('a', c);

	string s2("2");
	BOOST_REQUIRE(!parse(s2.begin(), s2.end(), Char::alpha<string>()));
}
