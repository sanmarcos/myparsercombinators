/*
 * charparsers.h
 *
 *  Created on: 20.01.2014
 *      Author: david
 */

#ifndef __MYPARSERLIB_CHARPARSERS_H_
#define __MYPARSERLIB_CHARPARSERS_H_

#include "core.h"
#include "lists.h"

#include <memory>
#include <string>
#include <vector>

namespace MyParserLib {
namespace Char {

template<typename Container>
typename Parser<typename Container::const_iterator, char>::ptr oneOf(const char *chars) {
	std::vector<char> charVector;
	for (const char *p = chars; *p != '\0'; ++p) {
		charVector.push_back(*p);
	}

	return std::make_shared < OneOf<typename Container::const_iterator, char> > (charVector);
}

template<typename Container>
typename Parser<typename Container::const_iterator, char>::ptr digit() {
	static typename Parser<typename Container::const_iterator, char>::ptr digitParser(NULL);
	if (!digitParser) {
		digitParser = oneOf<Container>("0123456789");
	}

	return digitParser;
}

template<typename Container>
typename Parser<typename Container::const_iterator, char>::ptr alpha() {
	static typename Parser<typename Container::const_iterator, char>::ptr alphaParser(NULL);
	if (!alphaParser) {
		alphaParser = oneOf<Container>("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	}

	return alphaParser;
}

template<typename Container>
typename Parser<typename Container::const_iterator, char>::ptr upper() {
	static typename Parser<typename Container::const_iterator, char>::ptr upperParser(NULL);
	if (!upperParser) {
		upperParser = oneOf<Container>("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	}

	return upperParser;
}

template<typename Container>
typename Parser<typename Container::const_iterator, char>::ptr lower() {
	static typename Parser<typename Container::const_iterator, char>::ptr lowerParser(NULL);
	if (!lowerParser) {
		lowerParser = oneOf<Container>("abcdefghijklmnopqrstuvwxyz");
	}

	return lowerParser;
}

template<typename Container>
typename Parser<typename Container::const_iterator, char>::ptr space() {
	static typename Parser<typename Container::const_iterator, char>::ptr lowerParser(NULL);
	if (!lowerParser) {
		lowerParser = oneOf<Container>(" \t\n\r\v\f");
	}

	return lowerParser;
}

template<typename Container>
typename Parser<typename Container::const_iterator, std::string>::ptr intPlain() {
	auto action = [] (std::vector<char> arg) {
		return std::string(arg.begin(), arg.end());
	};

	return apply(many(space<Container>())->_r(many1(digit<Container>())), action);
}

template<typename Container>
typename Parser<typename Container::const_iterator, int>::ptr int_() {
	auto action = [] (std::string s) {
		return stoi(s);
	};

	return apply(intPlain<Container>(), action);
}

}
}

#endif /* __MYPARSERLIB_CHARPARSERS_H_ */
