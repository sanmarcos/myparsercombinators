/*
 * test_lists.cpp
 *
 *  Created on: 11.02.2014
 *      Author: david
 */

#include "core.h"
#include "lists.h"

#include "charparsers.h"

using namespace MyParserLib;

#define BOOST_TEST_MODULE MyParserLib_Tokenizer test
#include <boost/test/included/unit_test.hpp>

#include <functional>
#include <string>
#include <vector>

using namespace std;

Parser<string::const_iterator, char>::ptr alpha = Char::alpha<string>();
Parser<string::const_iterator, char>::ptr digit = Char::digit<string>();
Parser<string::const_iterator, int>::ptr int_ = Char::int_<string>();
Parser<string::const_iterator, char>::ptr space = Char::space<string>();

BOOST_AUTO_TEST_CASE(many_test) {
	Parser<string::const_iterator, vector<char> >::ptr p = many(alpha)->_l(digit);
	vector<char> result;

	string s1("aaa1");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL(3, result.size());
	result.clear();

	string s2("1");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p, result));
	BOOST_CHECK_EQUAL(0, result.size());
	result.clear();

	string s3(" ");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));
}

BOOST_AUTO_TEST_CASE(many1_test) {
	Parser<string::const_iterator, vector<char> >::ptr p = many1(alpha)->_l(digit);
	vector<char> result;

	string s1("aaa1");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL(3, result.size());
	result.clear();

	string s2("a1");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p, result));
	BOOST_CHECK_EQUAL(1, result.size());
	result.clear();

	string s3("1");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));

	string s4(" ");
	BOOST_REQUIRE(!parse(s4.begin(), s4.end(), p));
}

BOOST_AUTO_TEST_CASE(sep1_test) {
	Parser<string::const_iterator, vector<char> >::ptr p = sepBy1(alpha, space)->_l(digit);
	vector<char> result;

	string s1("a a a1");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL(3, result.size());
	result.clear();

	string s2("a1");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p, result));
	BOOST_CHECK_EQUAL(1, result.size());
	result.clear();

	string s3("1");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));

	string s4(" ");
	BOOST_REQUIRE(!parse(s4.begin(), s4.end(), p));
}

BOOST_AUTO_TEST_CASE(sep_test) {
	Parser<string::const_iterator, vector<char> >::ptr p = sepBy(alpha, space)->_l(digit);
	vector<char> result;

	string s1("a a a1");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL(3, result.size());
	result.clear();

	string s2("1");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p, result));
	BOOST_CHECK_EQUAL(0, result.size());
	result.clear();

	string s3(" ");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));
}
