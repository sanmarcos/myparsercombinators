/*
 * myparserlib.h
 *
 *  Created on: 20.01.2014
 *      Author: david
 */

#ifndef __MYPARSERLIB_CORE_H_
#define __MYPARSERLIB_CORE_H_

#include <exception>
#include <functional>
#include <initializer_list>
#include <memory>
#include <string>
#include <vector>
#include <utility>

namespace MyParserLib {

template<typename Iterator>
class ParserState {
public:
	ParserState(Iterator _begin, Iterator _end) :
			begin(_begin), pos(_begin), end(_end) {
	}

	ParserState& operator=(const ParserState& other) {
		begin = other.begin;
		pos = other.pos;
		end = other.end;

		return *this;
	}

	Iterator getBegin() const {
		return begin;
	}

	Iterator getEnd() const {
		return end;
	}

	Iterator& getPos() const {
		return pos;
	}

	Iterator& getPos() {
		return pos;
	}

	unsigned getIndex() const {
		return pos - begin;
	}

	bool atEndOfInput() const {
		return pos == end;
	}

private:
	Iterator begin;
	Iterator pos;
	Iterator end;
};

struct Void {
};

class ParseException: public std::exception {
public:
	ParseException(unsigned _position) :
			position(_position) {
	}

	ParseException(unsigned _position, std::string _errorMessage) :
			position(_position), errorMessage(_errorMessage) {
	}

	ParseException(unsigned _position, const char *_errorMessage) :
			position(_position), errorMessage(_errorMessage) {
	}

	unsigned getPosition() const {
		return position;
	}

	std::string getErrorMessage() const {
		return errorMessage;
	}

private:
	unsigned position;
	std::string errorMessage;
};

template<typename Iterator, typename Result>
class Backtrack;

template<typename Iterator>
class Fail;

template<typename Iterator, typename Result, typename Ignored>
class Lookahead;

template<typename InnerParser, typename Callable>
class Map;

template<typename Iterator, typename Result, typename NewResult>
class Sequence;

template<typename Iterator, typename Result, typename NewResult>
class SequenceFirst;

template<typename Iterator, typename Result, typename NewResult>
class SequenceSecond;

template<typename Iterator, typename Result>
class Parser: public std::enable_shared_from_this<Parser<Iterator, Result>> {
private:
public:
	typedef std::shared_ptr<Parser<Iterator, Result>> ptr;
	typedef Result result_type;
	typedef Iterator iterator_type;

	virtual ~Parser() {
	}

	// Policy: No parse method is required to reset the state to before parsing began,
	// but it is also not illegal to do so.
	virtual void parse(ParserState<Iterator>&, Result& res) = 0;

	std::shared_ptr<Parser<Iterator, Result>> or_(std::shared_ptr<Parser<Iterator, Result>> other) {
		std::initializer_list<std::shared_ptr<Parser<Iterator, Result>>> l = {this->shared_from_this(), other};
		return std::make_shared < Backtrack<Iterator, Result>> (l);
	}

	std::shared_ptr<Parser<Iterator, Result>> if_(std::function<bool(Result)> predicate) {
		auto adapter = [predicate] (Result result, bool& fail) {
			fail = ! predicate(result);
			return result;
		};

		return std::make_shared<Map<Parser<Iterator, Result>, decltype(adapter)>>(this->shared_from_this(), adapter);
	}

	/**
	 * Concatenation with another parser. The resulting parser's result contains both parser's results.
	 */
	template<typename NewResult>
	std::shared_ptr<Parser<Iterator, std::pair<Result, NewResult>>> _(
			std::shared_ptr<Parser<Iterator, NewResult>> second) {
		return std::make_shared < Sequence<Iterator, Result, NewResult>> (this->shared_from_this(), second);
	}

	/**
	 * Concatenation with another parser. The resulting parser's result contains the first result.
	 */
	template<typename NewResult>
	std::shared_ptr<Parser<Iterator, Result>> _l(std::shared_ptr<Parser<Iterator, NewResult>> second) {
		return std::make_shared < SequenceFirst<Iterator, Result, NewResult>> (this->shared_from_this(), second);
	}

	/**
	 * Concatenation with another parser. The resulting parser's result contains the right result.
	 */
	template<typename NewResult>
	std::shared_ptr<Parser<Iterator, NewResult>> _r(std::shared_ptr<Parser<Iterator, NewResult>> second) {
		return std::make_shared < SequenceSecond<Iterator, Result, NewResult>> (this->shared_from_this(), second);
	}

	/**
	 * Concatenation. The result of the first parser is passed to the callable. This callable will create
	 * the second parser (or, better, return a previously generated one if possible).
	 */
	template<typename NewResult>
	std::shared_ptr<Parser<Iterator, std::pair<Result, NewResult>>> _(
			std::function<std::shared_ptr<Parser<Iterator, NewResult>>(Result)> callable) {

		auto adapter = [callable] (Result result) {
			return callable(result);
		};

		return std::make_shared < Sequence<Iterator, Result, NewResult>> (this->shared_from_this(), adapter);
	}

	/**
	 * Positive lookahead. If the predicate fails, the whole parser fails and the input won't be advanced.
	 */
	template<typename Ignored>
	std::shared_ptr<Parser<Iterator, Result>> followedBy(std::shared_ptr<Parser<Iterator, Ignored>> pred) {
		return std::make_shared < Lookahead<Iterator, Result, Ignored>> (this->shared_from_this(), pred, true);
	}

	/**
	 * Negative lookahead. If the predicate fails, the whole parser fails and the input won't be advanced.
	 */
	template<typename Ignored>
	std::shared_ptr<Parser<Iterator, Result>> notFollowedBy(std::shared_ptr<Parser<Iterator, Ignored>> pred) {
		return std::make_shared < Lookahead<Iterator, Result, Ignored>> (this->shared_from_this(), pred, false);
	}
};

template<typename InnerParser, typename Callable>
class Map: public Parser<typename InnerParser::iterator_type,
		typename std::result_of<Callable(typename InnerParser::result_type, bool&)>::type> {
public:
	Map(std::shared_ptr<InnerParser> _inner, Callable _actor) :
			inner(_inner), actor(_actor) {
	}

	virtual ~Map() {
	}

	virtual void parse(ParserState<typename InnerParser::iterator_type>& state,
			typename std::result_of<Callable(typename InnerParser::result_type, bool&)>::type& res) {
		typename InnerParser::result_type innerRes;
		inner->parse(state, innerRes);

		bool fail = false;
		res = actor(innerRes, fail);

		if (fail) {
			throw ParseException(state.getIndex());
		}
	}

private:
	typename InnerParser::ptr inner;
	Callable actor;
};

template<typename Iterator, typename Result>
class Any: public Parser<Iterator, Result> {
public:
	virtual ~Any() {
	}

	virtual void parse(ParserState<Iterator>& state, Result& result) {
		if (state.atEndOfInput()) {
			throw ParseException(state.getIndex(), "At end of input");
		}

		result = *state.getPos();
		++state.getPos();
	}
};

template<typename Iterator>
class Fail: public Parser<Iterator, Void> {
	virtual ~Fail() {
	}

	virtual void parse(ParserState<Iterator>& state, Void&) {
		throw ParseException(state.getIndex());
	}
};

template<typename Iterator, typename Result>
class Succeed: public Parser<Iterator, Result> {
public:
	Succeed() {
	}

	Succeed(Result _defaultResult) :
			defaultResult(_defaultResult) {
	}

	virtual ~Succeed() {
	}

	virtual void parse(ParserState<Iterator>&, Result& res) {
		res = defaultResult;
	}

private:
	Result defaultResult;
};

template<typename Iterator>
class Match: public Parser<Iterator, typename Iterator::value_type> {
public:
	Match(typename Iterator::value_type _expected) :
			expected(_expected) {
	}

	virtual ~Match() {
	}

	virtual void parse(ParserState<Iterator>& state, typename Iterator::value_type& res) {
		if (state.atEndOfInput()) {
			throw ParseException(state.getIndex(), "At end of input");
		}

		if (*state.getPos() != expected) {
			throw ParseException(state.getIndex(), "Unexpected symbol");
		}

		res = *state.getPos();
		++state.getPos();
	}

private:
	typename Iterator::value_type expected;
};

template<typename Iterator, typename First, typename Second>
class Sequence: public Parser<Iterator, std::pair<First, Second>> {
public:
	Sequence(std::shared_ptr<Parser<Iterator, First>> _first, std::shared_ptr<Parser<Iterator, Second>> _second) :
			firstParser(_first), callableForSecondParser([_second] (First) {return _second;}) {
		// In the second initializer the pointer is copied into the closure.
	}

	Sequence(std::shared_ptr<Parser<Iterator, First>> _first,
			std::function<std::shared_ptr<Parser<Iterator, Second>>(First)> _callable) :
			firstParser(_first), callableForSecondParser(_callable) {
	}

	virtual ~Sequence() {
	}

	virtual void parse(ParserState<Iterator>& state, std::pair<First, Second>& res) {
		firstParser->parse(state, res.first);
		typename Parser<Iterator, Second>::ptr second = callableForSecondParser(res.first);
		second->parse(state, res.second);
	}

private:
	typename Parser<Iterator, First>::ptr firstParser;
	std::function<typename Parser<Iterator, Second>::ptr(First)> callableForSecondParser;
};

template<typename Iterator, typename First, typename Second>
class SequenceFirst: public Parser<Iterator, First> {
public:
	SequenceFirst(typename Parser<Iterator, First>::ptr _first, typename Parser<Iterator, Second>::ptr _second) :
			firstParser(_first), callableForSecondParser([_second] (First) {return _second;}) {
		// In the second initializer the pointer is copied into the closure.
	}

	SequenceFirst(typename Parser<Iterator, First>::ptr _first,
			std::function<typename Parser<Iterator, Second>::ptr(First)> _callable) :
			firstParser(_first), callableForSecondParser(_callable) {
	}

	virtual ~SequenceFirst() {
	}

	virtual void parse(ParserState<Iterator>& state, First& firstRes) {
		Second secondRes;

		firstParser->parse(state, firstRes);
		typename Parser<Iterator, Second>::ptr second = callableForSecondParser(firstRes);
		second->parse(state, secondRes);
	}

private:
	typename Parser<Iterator, First>::ptr firstParser;
	std::function<typename Parser<Iterator, Second>::ptr(First)> callableForSecondParser;
};

template<typename Iterator, typename First, typename Second>
class SequenceSecond: public Parser<Iterator, Second> {
public:
	SequenceSecond(typename Parser<Iterator, First>::ptr _first, typename Parser<Iterator, Second>::ptr _second) :
			firstParser(_first), callableForSecondParser([_second] (First) {return _second;}) {
		// In the second initializer the pointer is copied into the closure.
	}

	SequenceSecond(typename Parser<Iterator, First>::ptr _first,
			std::function<typename Parser<Iterator, Second>::ptr(First)> _callable) :
			firstParser(_first), callableForSecondParser(_callable) {
	}

	virtual ~SequenceSecond() {
	}

	virtual void parse(ParserState<Iterator>& state, Second& secondRes) {
		First firstRes;

		firstParser->parse(state, firstRes);
		typename Parser<Iterator, Second>::ptr second = callableForSecondParser(firstRes);
		second->parse(state, secondRes);
	}

private:
	typename Parser<Iterator, First>::ptr firstParser;
	std::function<typename Parser<Iterator, Second>::ptr(First)> callableForSecondParser;
};

template<typename Iterator, typename TokenT>
class OneOf: public Parser<Iterator, TokenT> {
public:
	OneOf(std::vector<TokenT>& _options) :
			options(_options) {
	}

	OneOf(std::initializer_list<TokenT> _options) :
			options(_options.begin(), _options.end()) {
	}

	virtual ~OneOf() {
	}

	virtual void parse(ParserState<Iterator>& state, TokenT& res) {
		if (state.atEndOfInput()) {
			throw ParseException(state.getIndex(), "At end of input");
		}

		TokenT current = *state.getPos();

		typename std::vector<TokenT>::iterator currentOption = options.begin();
		for (; currentOption != options.end(); ++currentOption) {
			if (current == *currentOption) {
				res = current;
				++state.getPos();
				return;
			}
		}

		throw ParseException(state.getIndex());
	}

private:
	std::vector<TokenT> options;
};

template<typename Iterator, typename Result>
class Backtrack: public Parser<Iterator, Result> {
public:
	Backtrack(std::initializer_list<std::shared_ptr<Parser<Iterator, Result>>>_options) :
	options(_options.begin(), _options.end()) {
	}

	virtual ~Backtrack() {
	}

	virtual void parse(ParserState<Iterator>& state, Result& res) {
		for (auto it = options.begin(); it != options.end(); ++it) {
			std::shared_ptr<Parser<Iterator, Result>> parser = *it;

			try {
				ParserState<Iterator> copy(state);
				parser->parse(copy, res);
				state = copy;
				return;
			} catch (ParseException&) {
			}
		}

		throw ParseException(state.getIndex(), "Unexpected input");
	}

private:
	std::vector<std::shared_ptr<Parser<Iterator, Result>>> options;
};

template<typename Iterator, typename Result>
class Optional: public Parser<Iterator, Void> {
public:
	Optional(typename Parser<Iterator, Result>::ptr _inner) :
			inner(_inner) {
	}

	virtual ~Optional() {
	}

	virtual void parse(ParserState<Iterator>& state, Void&) {
		ParserState<Iterator> copy(state);
		Result res;

		try {
			inner->parse(copy, res);
			state = copy;
		} catch (ParseException&) {
		}
	}

private:
	typename Parser<Iterator, Result>::ptr inner;
};

template<typename Iterator, typename Result>
class Option: public Parser<Iterator, Result> {
public:
	Option(typename Parser<Iterator, Result>::ptr _inner, Result _defaultResult) :
			inner(_inner), defaultResult(_defaultResult) {
	}

	virtual ~Option() {
	}

	virtual void parse(ParserState<Iterator>& state, Result& res) {
		ParserState<Iterator> copy(state);

		try {
			inner->parse(copy, res);
			state = copy;
		} catch (ParseException&) {
			res = defaultResult;
		}
	}

private:
	typename Parser<Iterator, Result>::ptr inner;
	Result defaultResult;
};

template<typename Iterator, typename Result, typename Ignored>
class Lookahead: public Parser<Iterator, Result> {
public:
	Lookahead(typename Parser<Iterator, Result>::ptr _inner, typename Parser<Iterator, Ignored>::ptr _predicate,
			bool _accept) :
			inner(_inner), predicate(_predicate), accept(_accept) {
	}

	virtual ~Lookahead() {
	}

	virtual void parse(ParserState<Iterator>& state, Result& result) {
		inner->parse(state, result);

		// For error messages
		unsigned afterInnerParser = state.getIndex();

		Ignored ignoredResult;
		bool innerParserSucceeded = false;
		try {
			// The predicate shall not advance the parser position.
			ParserState<Iterator> copy(state);
			predicate->parse(copy, ignoredResult);
			innerParserSucceeded = true;
		} catch (ParseException& e) {
			if (accept) {
				throw e;
			}
		}

		if (innerParserSucceeded && !accept) {
			throw ParseException(afterInnerParser);
		}
	}

private:
	typename Parser<Iterator, Result>::ptr inner;
	typename Parser<Iterator, Ignored>::ptr predicate;
	const bool accept;
};

template<typename Iterator>
class EOI: public Parser<Iterator, Void> {
public:
	virtual ~EOI() {
	}

	virtual void parse(ParserState<Iterator>& state, Void&) {
		if (!state.atEndOfInput()) {
			throw ParseException(state.getIndex(), "End of input expected");
		}
	}
};

template<typename Iterator>
class String: public Parser<Iterator, Void> {
public:
	String(Iterator _begin, Iterator _end) :
			begin(_begin), end(_end) {
	}

	virtual ~String() {
	}

	virtual void parse(ParserState<Iterator>& state, Void&) {
		// For error message
		unsigned initialPosition = state.getIndex();
		Iterator beginCopy(begin);

		while (beginCopy != end && !state.atEndOfInput() && *beginCopy == *state.getPos()) {
			++beginCopy;
			++state.getPos();
		}

		if (beginCopy != end) {
			throw ParseException(initialPosition);
		}
	}

private:
	Iterator begin;
	Iterator end;
};

template<typename Iterator>
std::shared_ptr<Parser<Iterator, typename Iterator::value_type>> any() {
	return std::make_shared<Any<Iterator, typename Iterator::value_type>>();
}

/**
 * Transforms the parsing result. The passed function has the possibility to fail
 * the parsing process using the provided flag variable.
 */
template<typename InnerParser, typename Callable>
auto apply_fail(std::shared_ptr<InnerParser> parser, Callable actor)
-> std::shared_ptr<Parser<typename InnerParser::iterator_type,
typename std::result_of<Callable(typename InnerParser::result_type, bool&)>::type>> {
	typedef typename std::result_of<Callable(typename InnerParser::result_type, bool&)>::type NewResult;
	typedef typename InnerParser::result_type Result;
	typedef Map<InnerParser, Callable> ResultingParser;
	return std::make_shared < ResultingParser > (parser, actor);
}

/**
 * Transforms the parsing result.
 */
template<typename InnerParser, typename Callable>
auto apply(std::shared_ptr<InnerParser> parser, Callable actor)
-> std::shared_ptr<Parser<typename InnerParser::iterator_type,
typename std::result_of<Callable(typename InnerParser::result_type)>::type>> {
	auto wrapper = [actor] (typename InnerParser::result_type r, bool&) {
		return actor(r);
	};

	return apply_fail<InnerParser, decltype(wrapper)>(parser, wrapper);
}

template<typename Iterator>
std::shared_ptr<Parser<Iterator, Void>> eoi() {
	return std::make_shared<EOI<Iterator>>();
}

template<typename Iterator, typename CharT>
std::shared_ptr<Parser<Iterator, CharT>> match(CharT expected) {
	return std::make_shared < Match < Iterator >> (expected);
}

template<typename Iterator, typename CharT>
std::shared_ptr<Parser<Iterator, CharT>> match(std::initializer_list<CharT> options) {
	return std::make_shared<OneOf<Iterator, CharT>>(options);
}

template<typename Iterator>
std::shared_ptr<Parser<Iterator, Void>> match_string(Iterator begin, Iterator end) {
	return std::make_shared < String < Iterator >> (begin, end);
}

/**
 * Positive lookahead. The parsing is only successful if this parser parses. Doesn't consume input.
 */

template<typename InnerParser>
typename Parser<typename InnerParser::iterator_type, Void>::ptr lookahead(std::shared_ptr<InnerParser> parser) {
	auto succeed = std::make_shared<Succeed<typename InnerParser::iterator_type, Void>>();
	typedef Lookahead<typename InnerParser::iterator_type, Void, typename InnerParser::result_type> ResultingParser;
	return std::make_shared < ResultingParser > (succeed, parser, true);
}

/**
 * Negation. The parsing is only successful if this parser parses not. Doesn't consume input.
 */
template<typename InnerParser>
typename Parser<typename InnerParser::iterator_type, Void>::ptr not_(std::shared_ptr<InnerParser> parser) {
	typedef Lookahead<typename InnerParser::iterator_type, Void, typename InnerParser::result_type> ResultingParser;
	auto succeed = std::make_shared<Succeed<typename InnerParser::iterator_type, Void>>();
	return std::make_shared < ResultingParser > (succeed, parser, false);
}

template<typename Parser>
std::shared_ptr<Parser> option(std::shared_ptr<Parser> parser, typename Parser::result_type default_) {
	typedef Option<typename Parser::iterator_type, typename Parser::result_type> ResultingType;
	return std::make_shared < ResultingType > (parser, default_);
}

template<typename InnerParser>
std::shared_ptr<Parser<typename InnerParser::iterator_type, Void>> optional(std::shared_ptr<InnerParser> parser) {
	typedef Optional<typename InnerParser::iterator_type, typename InnerParser::result_type> ResultingType;
	return std::make_shared < ResultingType > (parser);
}

template<typename Parser>
bool parse(typename Parser::iterator_type begin, typename Parser::iterator_type end, std::shared_ptr<Parser> parser,
		typename Parser::result_type& result) {
	typename Parser::iterator_type afterParsing;
	return parse(begin, end, afterParsing, parser, result);
}

template<typename Parser>
bool parse(typename Parser::iterator_type begin, typename Parser::iterator_type end,
		typename Parser::iterator_type& afterParsing, std::shared_ptr<Parser> parser,
		typename Parser::result_type& result) {
	ParserState<typename Parser::iterator_type> init(begin, end);
	try {
		parser->parse(init, result);
		afterParsing = init.getPos();
		return true;
	} catch (ParseException&) {
		return false;
	}
}

template<typename Parser>
bool parse(typename Parser::iterator_type begin, typename Parser::iterator_type end, std::shared_ptr<Parser> parser) {
	typename Parser::result_type result;
	return parse(begin, end, parser, result);
}

template<typename Parser>
bool parse(typename Parser::iterator_type begin, typename Parser::iterator_type end,
		typename Parser::iterator_type& afterParsing, std::shared_ptr<Parser> parser) {
	typename Parser::result_type result;
	return parse(begin, end, afterParsing, parser, result);
}

template<typename Parser>
typename Parser::iterator_type parse_with_exception(typename Parser::iterator_type begin,
		typename Parser::iterator_type end, std::shared_ptr<Parser> parser, typename Parser::result_type& result) {
	ParserState<typename Parser::iterator_type> init(begin, end);
	parser->parse(init, result);
	return init.getPos();
}

template<typename Parser>
typename Parser::iterator_type parse_with_exception(typename Parser::iterator_type begin,
		typename Parser::iterator_type end, std::shared_ptr<Parser> parser) {
	typename Parser::result_type result;
	return parse_with_exception(begin, end, parser, result);
}

}

#endif /* __MYPARSERLIB_CORE_H_ */
