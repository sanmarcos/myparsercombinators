/*
 * core_test.cpp
 *
 *  Created on: 27.01.2014
 *      Author: David Kofler
 */

#include "core.h"

#include "charparsers.h"

using namespace MyParserLib;

#define BOOST_TEST_MODULE MyParserLib_Tokenizer test
#include <boost/test/included/unit_test.hpp>

#include <functional>
#include <string>
#include <vector>

using namespace std;

Parser<string::const_iterator, char>::ptr alpha = Char::alpha<string>();
Parser<string::const_iterator, char>::ptr digit = Char::digit<string>();
Parser<string::const_iterator, int>::ptr int_ = Char::int_<string>();
Parser<string::const_iterator, char>::ptr space = Char::space<string>();

BOOST_AUTO_TEST_CASE(string_test) {
	const string REF("ref");

	Parser<string::const_iterator, Void>::ptr p = match_string(REF.begin(), REF.end());

	string s1("ref");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p));

	string s2("refe");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p));

	string s3("arfasd");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));

	string s4("ar");
	BOOST_REQUIRE(!parse(s4.begin(), s4.end(), p));
}

BOOST_AUTO_TEST_CASE(match_single_test) {
	Parser<string::const_iterator, char>::ptr p = match<string::const_iterator>('a');
	char result;

	string s1("a");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL('a', result);

	string s2("b");
	BOOST_REQUIRE(!parse(s2.begin(), s2.end(), p));
}

BOOST_AUTO_TEST_CASE(if_test) {
	Parser<string::const_iterator, char>::ptr p1 = alpha->if_([](char) {
		return true;
	});

	char result;

	string s1("a");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p1, result));
	BOOST_CHECK_EQUAL('a', result);

	Parser<string::const_iterator, char>::ptr p2 = alpha->if_([](char) {
		return false;
	});

	string s2("b");
	BOOST_REQUIRE(!parse(s2.begin(), s2.end(), p2));
}

BOOST_AUTO_TEST_CASE(any_test) {
	Parser<string::const_iterator, pair<int, char>>::ptr p = int_->_(any<string::const_iterator>());
	pair<int, char> result;

	string s1("23a");
//	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_REQUIRE_NO_THROW(parse_with_exception(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL(23, result.first);
	BOOST_CHECK_EQUAL('a', result.second);

	string s2("23");
	BOOST_REQUIRE(!parse(s2.begin(), s2.end(), p));

	string s3("  ");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));
}

BOOST_AUTO_TEST_CASE(apply_callable_test) {
	Parser<string::const_iterator, int>::ptr p1 = apply(int_, [] (int i) -> int {
		return i + 1;
	});

	string s1("123");
	int result;
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p1, result));
	BOOST_CHECK_EQUAL(124, result);

	Parser<string::const_iterator, int>::ptr p2 = apply_fail(int_, [] (int i, bool& fail) {
		fail = true;
		return i;
	});

	BOOST_REQUIRE(!parse(s1.begin(), s1.end(), p2));
}

class Test {
public:
	Test(int _n) :
			n(_n) {
	}

	int add(int i) const {
		return i + n;
	}

	int error(int i, bool& fail) {
		fail = true;
		return i;
	}

private:
	int n;
};

BOOST_AUTO_TEST_CASE(apply_object_method_test) {
	const int OFFSET = 4;
	Test object(OFFSET);

//	function<int(int)> op1 = bind(mem_fn(&Test::add), object, _1);
	auto op1 = bind(mem_fn(&Test::add), object, _1);
	Parser<string::const_iterator, int>::ptr p1 = apply(int_, op1);

	string s1("123");
	int result;
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p1, result));
	BOOST_CHECK_EQUAL(123 + OFFSET, result);

	auto op2 = bind(mem_fn(&Test::error), object, _1, _2);
	Parser<string::const_iterator, int>::ptr p2 = apply_fail(int_, op2);

	BOOST_REQUIRE(!parse(s1.begin(), s1.end(), p2));
}

BOOST_AUTO_TEST_CASE(apply_function_test) {

}

BOOST_AUTO_TEST_CASE(optional_test) {
	Parser<string::const_iterator, char>::ptr p = optional(int_)->_r(space);
	char result;

	string s1("123\t");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL('\t', result);

	string s2("\t");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p, result));
	BOOST_CHECK_EQUAL('\t', result);

	string s3("123");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));

	string s4("bla");
	BOOST_REQUIRE(!parse(s4.begin(), s4.end(), p));
}

BOOST_AUTO_TEST_CASE(option_test) {
	const int DEFAULT_RESULT = 20;
	Parser<string::const_iterator, int>::ptr p = option(int_, DEFAULT_RESULT)->_l(alpha);
	int result;

	string s1("123a");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), p, result));
	BOOST_CHECK_EQUAL(123, result);

	string s2("a");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), p, result));
	BOOST_CHECK_EQUAL(DEFAULT_RESULT, result);

	string s3("1");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), p));
}

BOOST_AUTO_TEST_CASE(concatenation) {
	Parser<string::const_iterator, pair<char, char> >::ptr p = digit->_(alpha);
	pair<char, char> result;

	string source1("2a");
	BOOST_CHECK(parse(source1.begin(), source1.end(), p, result));
	BOOST_CHECK_EQUAL('2', result.first);
	BOOST_CHECK_EQUAL('a', result.second);

	string source2("ba");
	BOOST_CHECK(!parse(source2.begin(), source2.end(), p, result));

	string source3("22");
	BOOST_CHECK(!parse(source3.begin(), source3.end(), p, result));
}

BOOST_AUTO_TEST_CASE(concatenation_left) {
	Parser<string::const_iterator, char>::ptr p = digit->_l(alpha);
	char result;

	string source1("2a");
	BOOST_CHECK(parse(source1.begin(), source1.end(), p, result));
	BOOST_CHECK_EQUAL('2', result);

	string source2("ba");
	BOOST_CHECK(!parse(source2.begin(), source2.end(), p, result));

	string source3("22");
	BOOST_CHECK(!parse(source3.begin(), source3.end(), p, result));
}

BOOST_AUTO_TEST_CASE(concatenation_right) {
	Parser<string::const_iterator, char>::ptr p = digit->_r(alpha);
	char result;

	string source1("2a");
	BOOST_CHECK(parse(source1.begin(), source1.end(), p, result));
	BOOST_CHECK_EQUAL('a', result);

	string source2("ba");
	BOOST_CHECK(!parse(source2.begin(), source2.end(), p, result));

	string source3("22");
	BOOST_CHECK(!parse(source3.begin(), source3.end(), p, result));
}

BOOST_AUTO_TEST_CASE(positive_lookahead) {
	string source1("23b");

	Parser<string::const_iterator, int>::ptr p = int_->followedBy(space);
	BOOST_CHECK(!parse(source1.begin(), source1.end(), p));

	string source2("23 ");
	int result;
	BOOST_CHECK(parse(source2.begin(), source2.end(), p, result));
	BOOST_CHECK_EQUAL(23, result);
}

BOOST_AUTO_TEST_CASE(initial_positive_lookahead) {
	Parser<string::const_iterator, char>::ptr aParser = Char::oneOf<string>("a");
	Parser<string::const_iterator, char>::ptr p = lookahead(aParser)->_r(alpha);
	char result;

	string source1("a");
	BOOST_CHECK(parse(source1.begin(), source1.end(), p, result));
	BOOST_CHECK_EQUAL('a', result);

	string source2("b");
	BOOST_CHECK(!parse(source2.begin(), source2.end(), p));

	string source3("");
	BOOST_CHECK(!parse(source3.begin(), source3.end(), p));
}

BOOST_AUTO_TEST_CASE(negative_lookahead) {
	Parser<string::const_iterator, int>::ptr p = int_->notFollowedBy(space);
	int result;

	string source1("23");
	BOOST_CHECK(parse(source1.begin(), source1.end(), p, result));
	BOOST_CHECK_EQUAL(23, result);

	string source2("43b");
	BOOST_CHECK(parse(source2.begin(), source2.end(), p, result));
	BOOST_CHECK_EQUAL(43, result);

	string source3("43 ");
	BOOST_CHECK(!parse(source3.begin(), source3.end(), p));
}

BOOST_AUTO_TEST_CASE(initial_negative_lookahead) {
	Parser<string::const_iterator, int>::ptr p = not_(alpha)->_r(int_);
	int result;

	string source1("23");
	BOOST_CHECK(parse(source1.begin(), source1.end(), p, result));
	BOOST_CHECK_EQUAL(23, result);

	string source2("j");
	BOOST_CHECK(!parse(source2.begin(), source2.end(), p));

	string source3("");
	BOOST_CHECK(!parse(source3.begin(), source3.end(), p));
}

BOOST_AUTO_TEST_CASE(after_parsing_iterator) {
	const string REF("ref");

	Parser<string::const_iterator, Void>::ptr p = match_string(REF.begin(), REF.end());

	string s1("ref");
	BOOST_REQUIRE(parse_with_exception(s1.begin(), s1.end(), p) == s1.end());

	string s2("refa");
	BOOST_REQUIRE(parse_with_exception(s2.begin(), s2.end(), p) == s2.begin() + 3);
}
