/*
 * sequences.h
 *
 *  Created on: 11.02.2014
 *      Author: David Kofler
 */

#ifndef __MYPARSERLIB_LISTS_H_
#define __MYPARSERLIB_LISTS_H_

#include "core.h"

#include <memory>
#include <vector>

namespace MyParserLib {

template<typename Iterator, typename Result>
class Many: public Parser<Iterator, std::vector<Result> > {
public:
	Many(typename Parser<Iterator, Result>::ptr _inner) :
			inner(_inner) {
	}

	virtual ~Many() {
	}

	virtual void parse(ParserState<Iterator>& state, std::vector<Result>& res) {
		Result innerRes;
		ParserState<Iterator> copy(state);

		while (true) {
			try {
				inner->parse(copy, innerRes);
				res.push_back(innerRes);
			} catch (ParseException&) {
				break;
			}
		}

		state = copy;
	}

private:
	typename Parser<Iterator, Result>::ptr inner;
};

template<typename Iterator, typename Result>
class Many1: public Parser<Iterator, std::vector<Result> > {
public:
	Many1(typename Parser<Iterator, Result>::ptr _inner) :
			inner(_inner) {
	}

	virtual ~Many1() {
	}

	virtual void parse(ParserState<Iterator>& state, std::vector<Result>& res) {
		Result innerRes;
		inner->parse(state, innerRes);
		res.push_back(innerRes);

		// After parsing, the iterator should point to the position right after the
		// parsed input.
		ParserState<Iterator> copy(state);
		while (true) {
			try {
				inner->parse(copy, innerRes);
				res.push_back(innerRes);
			} catch (ParseException&) {
				break;
			}
		}

		state = copy;
	}

private:
	typename Parser<Iterator, Result>::ptr inner;
};

/**
 * Greedy Kleene closure.
 */
template<typename Iterator, typename Result>
std::shared_ptr<Parser<Iterator, std::vector<Result> > > many(std::shared_ptr<Parser<Iterator, Result> > parser) {
	return std::make_shared < Many<Iterator, Result> > (parser);
}

/**
 * Greedy Kleene closure. At least one occurrence is required.
 */
template<typename Iterator, typename Result>
std::shared_ptr<Parser<Iterator, std::vector<Result> > > many1(std::shared_ptr<Parser<Iterator, Result> > parser) {
	return std::make_shared < Many1<Iterator, Result> > (parser);
}

template<typename Iterator, typename Result, typename Ignored>
std::shared_ptr<Parser<Iterator, std::vector<Result> > > sepBy(std::shared_ptr<Parser<Iterator, Result> > parser,
		std::shared_ptr<Parser<Iterator, Ignored> > separator) {
	return option(sepBy1(parser, separator), std::vector<Result>());
}

template<typename Iterator, typename Result, typename Ignored>
std::shared_ptr<Parser<Iterator, std::vector<Result> > > sepBy1(std::shared_ptr<Parser<Iterator, Result> > parser,
		std::shared_ptr<Parser<Iterator, Ignored> > separator) {
	auto listItem = separator->_r(parser);
	auto combine = [] (std::pair<Result, std::vector<Result>> t) {
		t.second.insert(t.second.begin(), t.first);
		return t.second;
	};

	return apply(parser->_(many(listItem)), combine);
}

}

#endif /* __MYPARSERLIB_LISTS_H_ */
