/*
 * tokenizer_test.cpp
 *
 *  Created on: 27.01.2014
 *      Author: David Kofler
 */

#include "tokenizer.h"

#include "core.h"
#include "charparsers.h"

using namespace MyParserLib;

#define BOOST_TEST_MODULE MyParserLib_Tokenizer test
#include <boost/test/included/unit_test.hpp>

#include <functional>
#include <string>
#include <vector>

using namespace std;

typedef vector<Token<char, int> >::iterator TokenIterator_t;

BOOST_AUTO_TEST_CASE(tokenizer) {
	string source("2d 2");
	Tokenizer<string::const_iterator, int> tokenizer;

	tokenizer.addRule(Char::digit<string>(), 0);
	tokenizer.addRule(Char::alpha<string>(), 1);
	tokenizer.addRule(Char::space<string>(), 2);

	vector<Token<char, int> > tokens;
	tokenizer.getTokens(source.begin(), source.end(), tokens);

	TokenIterator_t it = tokens.begin();

	BOOST_REQUIRE(tokens.end() != it);
	Token<char, int> result(*it);
	BOOST_CHECK_EQUAL(0, result.getTag());
	BOOST_CHECK_EQUAL("2", result.getContent());

	++it;
	BOOST_REQUIRE(tokens.end() != it);
	result = *it;
	BOOST_CHECK_EQUAL(1, result.getTag());
	BOOST_CHECK_EQUAL("d", result.getContent());

	++it;
	BOOST_REQUIRE(tokens.end() != it);
	result = *it;
	BOOST_CHECK_EQUAL(2, result.getTag());
	BOOST_CHECK_EQUAL(" ", result.getContent());

	++it;
	BOOST_REQUIRE(tokens.end() != it);
	result = *it;
	BOOST_CHECK_EQUAL(0, result.getTag());
	BOOST_CHECK_EQUAL("2", result.getContent());

	++it;
	BOOST_REQUIRE(it == tokens.end());
}

BOOST_AUTO_TEST_CASE(tokenizer_rule_order) {
	Tokenizer<string::const_iterator, int> tokenizer;
	tokenizer.addRule(Char::oneOf<string>("2"), 0);
	tokenizer.addRule(Char::oneOf<string>("2"), 1);

	string source("2");
	vector<Token<char, int> > tokens;
	BOOST_REQUIRE(tokenizer.getTokens(source.begin(), source.end(), tokens));
}

BOOST_AUTO_TEST_CASE(withTag_token_parser) {
	vector<Token<char, int>> tokens;
	tokens.emplace_back(0, "", 0);

	Token<char, int> result;
	Parser<TokenIterator_t, Token<char, int>>::ptr p1 = token<TokenIterator_t>(0);
	BOOST_REQUIRE(parse(tokens.begin(), tokens.end(), p1, result));
	BOOST_CHECK_EQUAL("", result.getContent());
	BOOST_CHECK_EQUAL(0, result.getTag());

	Parser<TokenIterator_t, Token<char, int>>::ptr p2 = token<TokenIterator_t>(1);
	BOOST_REQUIRE(!parse(tokens.begin(), tokens.end(), p2));

	Parser<TokenIterator_t, Token<char, int>>::ptr p3 = token<TokenIterator_t>( { 0, 1 });
	BOOST_REQUIRE(parse(tokens.begin(), tokens.end(), p3, result));
	BOOST_CHECK_EQUAL("", result.getContent());
	BOOST_CHECK_EQUAL(0, result.getTag());

	Parser<TokenIterator_t, Token<char, int>>::ptr p4 = token<TokenIterator_t>( { 1, 2 });
	BOOST_REQUIRE(!parse(tokens.begin(), tokens.end(), p4));
}
