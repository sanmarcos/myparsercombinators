/*
 * test_grammar.cpp
 *
 *  Created on: 27.01.2014
 *      Author: David Kofler
 */

#include "grammar.h"

#include "core.h"
#include "charparsers.h"

using namespace MyParserLib;

#define BOOST_TEST_MODULE MyParserLib_Grammar test
#include <boost/test/included/unit_test.hpp>

#include <functional>
#include <list>
#include <memory>
#include <string>
using namespace std;

class Test4Grammar: public Grammar<string::const_iterator, int> {
public:
	Test4Grammar() {
		init("expr");

		Parser<string::const_iterator, char>::ptr openingBrace = match<string::const_iterator>( { '(' });
		Parser<string::const_iterator, char>::ptr closingBrace = match<string::const_iterator>( { ')' });
		insert("expr", getRule("int")->or_(openingBrace->_r(getRule("expr"))->_l(closingBrace)));
		insert("int", Char::int_<string>());
	}
};

BOOST_AUTO_TEST_CASE(grammar_test) {
	Test4Grammar::ptr grammar = make_shared<Test4Grammar>();
	int result;

	string s1("123");
	BOOST_REQUIRE(parse(s1.begin(), s1.end(), grammar, result));
	BOOST_CHECK_EQUAL(123, result);

	string s2("((123))");
	BOOST_REQUIRE(parse(s2.begin(), s2.end(), grammar, result));
	BOOST_CHECK_EQUAL(123, result);

	string s3("(123");
	BOOST_REQUIRE(!parse(s3.begin(), s3.end(), grammar));
}
